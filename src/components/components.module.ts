import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { SkeletonCardComponent } from './skeleton-card/skeleton-card';

@NgModule({
	declarations: [ SkeletonCardComponent ],
	imports: [ IonicModule ],
	exports: [ SkeletonCardComponent ]
})
export class ComponentsModule {}
